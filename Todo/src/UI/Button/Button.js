import React from 'react';

function Button({children,...props}) {
    return (
        <button {...props} className="btn btn-outline-success  w-100 ">
            {children}
        </button>
    );
}

export default Button;