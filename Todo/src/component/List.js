import React from 'react';
import Body from "./Body";
import {CSSTransition, TransitionGroup} from "react-transition-group";


const List = ({posts, title, remov}) => {
    return (
        <div className="list">
            <h5 className="text-center">{title}</h5>
            <TransitionGroup className="group">
                {posts.map((post, index) => (
                    <CSSTransition
                        key={post.id}
                        timeout={500}
                        classNames="salom"
                    >
                        <Body remov={remov} number={index + 1} post={post}/>
                    </CSSTransition>
                ))}
            </TransitionGroup>
        </div>
    );
};

export default List;