import React, {useState} from 'react';

function Router() {
    const [name, setname] = useState(false)
    const editname = (e) => setname(e.target.value)
    const [count, setcount] = useState(0)

    function qosh() {
        setcount(prev => prev + 2)
    }

    function ayir() {
        setcount(prev => prev - 2)
    }

    return (
        <div>
            <div className="container mb-4">
                <div className="row">
                    <div className="col-md-6 offset-3">
                        <div className="input-group">
                            <input type="text" className="form-control" placeholder="Royxatan otish" value={name}
                                   onChange={editname}/>
                            <div className="input-group-append">
                                <button type="submit" className="btn btn-outline-success"
                                        onClick={() => setname(!setname)}>Qoshish
                                </button>
                            </div>
                        </div>
                        <h1>count:{count}</h1>
                        <button type="submit" className="btn btn-success" onClick={qosh}>Qoshish</button>
                        <button type="submit" className="btn btn-success mx-3" onClick={ayir}>Ayrish</button>
                        {name ?
                            <div className="card-header mt-3">
                                <h1 className="text-warning">salom dunyo</h1>
                            </div>
                            : undefined
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Router;